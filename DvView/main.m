//
//  main.m
//  DvView
//
//  Created by User on 13年1月25日.
//  Copyright (c) 2013年 User. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DvAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DvAppDelegate class]));
    }
}
